import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: ""
  },
  mutations: {
    setToken(state, newToken){
      state.token = newToken
      localStorage.setItem("token", newToken)
      axios.defaults.headers.Authorization = "Bearer "+newToken
    },
    logout(state) {
      state.token = ""
      localStorage.removeItem("token")
      axios.defaults.headers.Authorization = null
    }
  },
  getters: {
    isConnected(state) {
      /*if(state.token !== "") {
        return  true
      } else {
        return false
      }*/
      return state.token !== ""
    }
  }
})
