import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import '@babel/polyfill'
import axios from 'axios';

axios.defaults.baseURL=process.env.VUE_APP_API_URL

const token = localStorage.getItem("token")
if(token){
  store.commit("setToken", token)
}

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
